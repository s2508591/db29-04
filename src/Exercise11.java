import java.sql.*;

public class Exercise11 {
    public static void main(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        String host = "bronto.ewi.utwente.nl";
        String dbName = " ";
        String url = "jdbc:postgresql://"
                + host + ":5432/" + dbName;
        String username = " ";
        String password = " ";
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqle) {
            System.err.println("Error connecting: " + sqle);
        }
        try {
            int iters=100;
            long startTime = System.currentTimeMillis();
            for (int i=0; i<iters; i++) {
                Statement statement =
                        connection.createStatement();
                String query = "SELECT p.name\n" +
                        "FROM movies.person p, movies.movie m, movies.writes w\n" +
                        "WHERE p.pid = w.pid\n" +
                        "AND m.mid = w.mid\n" +
                        "AND NOT EXISTS\n" +
                        "(\n" +
                        "SELECT *\n" +
                        "FROM movies.directs d, movies.writes w2\n" +
                        "WHERE w2.mid = d.mid\n" +
                        "AND w2.pid = p.pid\n" +
                        ")\n";
                ResultSet resultSet =
                        statement.executeQuery(query);
            }
            long stopTime = System.currentTimeMillis();
            double elapsedTime = (stopTime - startTime) / (1.0*iters);
            System.out.println("Measured time: "+elapsedTime+" ms");

            startTime = System.currentTimeMillis();
            String query = "SELECT p.name\n" +
                    "FROM movies.person p, movies.movie m, movies.writes w\n" +
                    "WHERE p.pid = w.pid\n" +
                    "AND m.mid = w.mid\n" +
                    "AND NOT EXISTS\n" +
                    "(\n" +
                    "SELECT *\n" +
                    "FROM movies.directs d, movies.writes w2\n" +
                    "WHERE w2.mid = d.mid\n" +
                    "AND w2.pid = p.pid\n" +
                    ")\n";
            for (int i=0; i<iters; i++) {
                PreparedStatement st =
                        connection.prepareStatement(query);
                ResultSet resultSet =
                        st.executeQuery();
            }
            stopTime = System.currentTimeMillis();
            elapsedTime = (stopTime - startTime) / (1.0*iters);

            System.out.println("Measured time: "+elapsedTime+" ms");

            startTime = System.currentTimeMillis();
            for (int i=0; i<iters; i++) {
                Statement statement =
                        connection.createStatement();
                query = "SELECT Exercise11('Bruce Willis')";
                ResultSet resultSet =
                        statement.executeQuery(query);
            }
            stopTime = System.currentTimeMillis();
            elapsedTime = (stopTime - startTime) / (1.0*iters);

            System.out.println("Measured time: "+elapsedTime+" ms");
            connection.close();
        } catch (java.sql.SQLException e) {
            System.out.println(e);
        }
    }
}