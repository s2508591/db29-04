import java.sql.*;

public class Exercise6 {
    public static void main(String[] args) {
        ex6("Bruce Willis");
    }
    private static void ex6(String name) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        String host = "bronto.ewi.utwente.nl";
        String dbName = " ";
        String url = "jdbc:postgresql://"
                + host + ":5432/" + dbName;
        String username = " ";
        String password = " ";
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqle) {
            System.err.println("Error connecting: " + sqle);
        }
        try {
            String query = "SELECT DISTINCT p.name\n" +
                    "FROM movies.person p, movies.writes w, movies.movie m, movies.acts a, movies.person p2\n" +
                    "WHERE p2.name = ?\n" +
                    "AND m.mid = a.mid\n" +
                    "AND m.mid = w.mid\n" +
                    "AND w.pid = p.pid\n" +
                    "AND a.pid = p2.pid\n";
            PreparedStatement st =
                    connection.prepareStatement(query);
            st.setString(1, name);
            ResultSet resultSet =
                    st.executeQuery();
            while(resultSet.next()) {
                System.out.println(resultSet.getString(1));
            }
            connection.close();
        } catch (java.sql.SQLException e) {
            System.out.println(e);
        }
    }
}