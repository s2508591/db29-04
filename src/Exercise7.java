import java.sql.*;

public class Exercise7 {
    public static void main(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        String host = "bronto.ewi.utwente.nl";
        String dbName = " ";
        String url = "jdbc:postgresql://"
                + host + ":5432/" + dbName;
        String username = " ";
        String password = " ";
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqle) {
            System.err.println("Error connecting: " + sqle);
        }
        try {
            Statement statement =
                    connection.createStatement();
            String query = "SELECT MoviesOfActor('Bruce Willis')";
            ResultSet resultSet =
                    statement.executeQuery(query);
            while(resultSet.next()) {
                System.out.println(resultSet.getString(1) + " ");
            }
            connection.close();
        } catch (java.sql.SQLException e) {
            System.out.println(e);
        }
    }
}